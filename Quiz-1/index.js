// soal 1
var tanggal = 29;
var bulan = 2;
var tahun = 2022;
function next_data(tanggal, bulan, tahun) {
    var hasil = new Date();
    tanggal = String(hasil.getDate()).padStart(2, '0');
    bulan = String(hasil.getMonth() + 1).padStart(2, '0');
    tahun = hasil.getFullYear();

    hasil = bulan + ' ' + tanggal + ' ' + tahun;
    return hasil;
}
console.log(next_data(tanggal, bulan, tahun));
// soal 2
var kalimat = "Halo nama saya Mohammad Luthfi Ismail Valent";
function jumlah_kata() {
    kalimat = kalimat.replace(/(^\s*)|(\s*$)/gi,"");
    kalimat = kalimat.replace(/[ ]{2,}/gi," ");
    kalimat = kalimat.replace(/\n /,"\n");
    return kalimat.split(' ').filter(function(n) {return n != ''}).length;
}

console.log(jumlah_kata(kalimat));