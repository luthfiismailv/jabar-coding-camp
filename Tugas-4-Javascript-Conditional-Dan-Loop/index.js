console.log("------Soal 1------");
// soal 1
var nilai = 69; // inisiasi variabel
if (nilai >= 85) {
    console.log("A");
} else if (nilai >= 75 && nilai < 85) {
    console.log("B");
} else if (nilai >= 65 && nilai < 75) {
    console.log("C"); // jawaban soal 1
} else if (nilai >= 55 && nilai < 65) {
    console.log("D");
} else {
    console.log("E");
}

console.log("------Soal 2------");
// soal 2
var tanggal = 14;
var bulan = 2;
var tahun = 2003;
switch (bulan) {
    case 1:
        console.log("14 Januari 2003");
        break;
    case 2:
        console.log("14 Februari 2003"); // jawaban soal 2
        break;
    case 3:
        console.log("14 Maret 2003");
        break;
    case 4:
        console.log("14 April 2003");
        break;          
    default:
        console.log("Pilihan tidak ada");
        break;
}

console.log("------Soal 3------");
// soal 3
var n = 7;
var hasil = "#";

console.log("nilai n = " + n);
// jawaban soal 3
while (hasil.length <= n) {
    console.log(hasil); 
    hasil += "#";
}

console.log("------Soal 4------");
// soal 4
var nomor = 1;
var array = ["I love programming", "I love Javascript", "I love VueJS"];
var batas = "===";

var m = 10;
var sisa = m % 3;
var batasan = ( m - sisa) / 3;

console.log("nilai m = " + m);
// jawaban soal 4
for (var i = 1; i <= batasan; i++) {
    for (var j = 0; j < 3; j++) {
        switch (j) {
            case 0:
                console.log(nomor + " - " + "I love programming");
                break;
            case 1:
                console.log(nomor + " - " + "I love Javascript");
                break;
            case 2:
                console.log(nomor + " - " + "I love VueJS");
                break;
        }
        nomor++;
    }
    if (i < batasan || sisa > 0) {
        for (var a = 0; a <= i; a = batas += "===") {
            console.log(batas);
        }
    }
}

if (sisa > 0) {
    for (var b = 0; b < sisa; b++) {
        switch (b) {
            case 0:
                console.log(nomor + " - " + "I love programming");
                break;
            case 1:
                console.log(nomor + " - " + "I love Javascript");
                break;
            case 2:
                console.log(nomor + " - " + "I love VueJS");
                break;
        }
        nomor++;
    }
}